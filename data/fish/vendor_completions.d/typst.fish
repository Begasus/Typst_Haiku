complete -c typst -n "__fish_use_subcommand" -l color -d 'Set when to use color. auto = use color if a capable terminal is detected' -r -f -a "{auto	'',always	'',never	''}"
complete -c typst -n "__fish_use_subcommand" -l cert -d 'Path to a custom CA certificate to use when making network requests' -r -F
complete -c typst -n "__fish_use_subcommand" -s h -l help -d 'Print help'
complete -c typst -n "__fish_use_subcommand" -s V -l version -d 'Print version'
complete -c typst -n "__fish_use_subcommand" -f -a "compile" -d 'Compiles an input file into a supported output format'
complete -c typst -n "__fish_use_subcommand" -f -a "watch" -d 'Watches an input file and recompiles on changes'
complete -c typst -n "__fish_use_subcommand" -f -a "init" -d 'Initializes a new project from a template'
complete -c typst -n "__fish_use_subcommand" -f -a "query" -d 'Processes an input file to extract provided metadata'
complete -c typst -n "__fish_use_subcommand" -f -a "fonts" -d 'Lists all discovered fonts in system and custom font paths'
complete -c typst -n "__fish_use_subcommand" -f -a "update" -d 'Self update the Typst CLI (disabled)'
complete -c typst -n "__fish_use_subcommand" -f -a "help" -d 'Print this message or the help of the given subcommand(s)'
complete -c typst -n "__fish_seen_subcommand_from compile" -l root -d 'Configures the project root (for absolute paths)' -r -F
complete -c typst -n "__fish_seen_subcommand_from compile" -l input -d 'Add a string key-value pair visible through `sys.inputs`' -r
complete -c typst -n "__fish_seen_subcommand_from compile" -l font-path -d 'Adds additional directories to search for fonts' -r -F
complete -c typst -n "__fish_seen_subcommand_from compile" -l diagnostic-format -d 'The format to emit diagnostics in' -r -f -a "{human	'',short	''}"
complete -c typst -n "__fish_seen_subcommand_from compile" -s f -l format -d 'The format of the output file, inferred from the extension by default' -r -f -a "{pdf	'',png	'',svg	''}"
complete -c typst -n "__fish_seen_subcommand_from compile" -l open -d 'Opens the output file using the default viewer after compilation' -r
complete -c typst -n "__fish_seen_subcommand_from compile" -l ppi -d 'The PPI (pixels per inch) to use for PNG export' -r
complete -c typst -n "__fish_seen_subcommand_from compile" -l timings -d 'Produces performance timings of the compilation process (experimental)' -r -F
complete -c typst -n "__fish_seen_subcommand_from compile" -s h -l help -d 'Print help (see more with \'--help\')'
complete -c typst -n "__fish_seen_subcommand_from watch" -l root -d 'Configures the project root (for absolute paths)' -r -F
complete -c typst -n "__fish_seen_subcommand_from watch" -l input -d 'Add a string key-value pair visible through `sys.inputs`' -r
complete -c typst -n "__fish_seen_subcommand_from watch" -l font-path -d 'Adds additional directories to search for fonts' -r -F
complete -c typst -n "__fish_seen_subcommand_from watch" -l diagnostic-format -d 'The format to emit diagnostics in' -r -f -a "{human	'',short	''}"
complete -c typst -n "__fish_seen_subcommand_from watch" -s f -l format -d 'The format of the output file, inferred from the extension by default' -r -f -a "{pdf	'',png	'',svg	''}"
complete -c typst -n "__fish_seen_subcommand_from watch" -l open -d 'Opens the output file using the default viewer after compilation' -r
complete -c typst -n "__fish_seen_subcommand_from watch" -l ppi -d 'The PPI (pixels per inch) to use for PNG export' -r
complete -c typst -n "__fish_seen_subcommand_from watch" -l timings -d 'Produces performance timings of the compilation process (experimental)' -r -F
complete -c typst -n "__fish_seen_subcommand_from watch" -s h -l help -d 'Print help (see more with \'--help\')'
complete -c typst -n "__fish_seen_subcommand_from init" -s h -l help -d 'Print help (see more with \'--help\')'
complete -c typst -n "__fish_seen_subcommand_from query" -l root -d 'Configures the project root (for absolute paths)' -r -F
complete -c typst -n "__fish_seen_subcommand_from query" -l input -d 'Add a string key-value pair visible through `sys.inputs`' -r
complete -c typst -n "__fish_seen_subcommand_from query" -l font-path -d 'Adds additional directories to search for fonts' -r -F
complete -c typst -n "__fish_seen_subcommand_from query" -l diagnostic-format -d 'The format to emit diagnostics in' -r -f -a "{human	'',short	''}"
complete -c typst -n "__fish_seen_subcommand_from query" -l field -d 'Extracts just one field from all retrieved elements' -r
complete -c typst -n "__fish_seen_subcommand_from query" -l format -d 'The format to serialize in' -r -f -a "{json	'',yaml	''}"
complete -c typst -n "__fish_seen_subcommand_from query" -l one -d 'Expects and retrieves exactly one element'
complete -c typst -n "__fish_seen_subcommand_from query" -s h -l help -d 'Print help'
complete -c typst -n "__fish_seen_subcommand_from fonts" -l font-path -d 'Adds additional directories to search for fonts' -r -F
complete -c typst -n "__fish_seen_subcommand_from fonts" -l variants -d 'Also lists style variants of each font family'
complete -c typst -n "__fish_seen_subcommand_from fonts" -s h -l help -d 'Print help'
complete -c typst -n "__fish_seen_subcommand_from update" -l force -d 'Forces a downgrade to an older version (required for downgrading)'
complete -c typst -n "__fish_seen_subcommand_from update" -l revert -d 'Reverts to the version from before the last update (only possible if `typst update` has previously ran)'
complete -c typst -n "__fish_seen_subcommand_from update" -s h -l help -d 'Print help'
complete -c typst -n "__fish_seen_subcommand_from help; and not __fish_seen_subcommand_from compile; and not __fish_seen_subcommand_from watch; and not __fish_seen_subcommand_from init; and not __fish_seen_subcommand_from query; and not __fish_seen_subcommand_from fonts; and not __fish_seen_subcommand_from update; and not __fish_seen_subcommand_from help" -f -a "compile" -d 'Compiles an input file into a supported output format'
complete -c typst -n "__fish_seen_subcommand_from help; and not __fish_seen_subcommand_from compile; and not __fish_seen_subcommand_from watch; and not __fish_seen_subcommand_from init; and not __fish_seen_subcommand_from query; and not __fish_seen_subcommand_from fonts; and not __fish_seen_subcommand_from update; and not __fish_seen_subcommand_from help" -f -a "watch" -d 'Watches an input file and recompiles on changes'
complete -c typst -n "__fish_seen_subcommand_from help; and not __fish_seen_subcommand_from compile; and not __fish_seen_subcommand_from watch; and not __fish_seen_subcommand_from init; and not __fish_seen_subcommand_from query; and not __fish_seen_subcommand_from fonts; and not __fish_seen_subcommand_from update; and not __fish_seen_subcommand_from help" -f -a "init" -d 'Initializes a new project from a template'
complete -c typst -n "__fish_seen_subcommand_from help; and not __fish_seen_subcommand_from compile; and not __fish_seen_subcommand_from watch; and not __fish_seen_subcommand_from init; and not __fish_seen_subcommand_from query; and not __fish_seen_subcommand_from fonts; and not __fish_seen_subcommand_from update; and not __fish_seen_subcommand_from help" -f -a "query" -d 'Processes an input file to extract provided metadata'
complete -c typst -n "__fish_seen_subcommand_from help; and not __fish_seen_subcommand_from compile; and not __fish_seen_subcommand_from watch; and not __fish_seen_subcommand_from init; and not __fish_seen_subcommand_from query; and not __fish_seen_subcommand_from fonts; and not __fish_seen_subcommand_from update; and not __fish_seen_subcommand_from help" -f -a "fonts" -d 'Lists all discovered fonts in system and custom font paths'
complete -c typst -n "__fish_seen_subcommand_from help; and not __fish_seen_subcommand_from compile; and not __fish_seen_subcommand_from watch; and not __fish_seen_subcommand_from init; and not __fish_seen_subcommand_from query; and not __fish_seen_subcommand_from fonts; and not __fish_seen_subcommand_from update; and not __fish_seen_subcommand_from help" -f -a "update" -d 'Self update the Typst CLI (disabled)'
complete -c typst -n "__fish_seen_subcommand_from help; and not __fish_seen_subcommand_from compile; and not __fish_seen_subcommand_from watch; and not __fish_seen_subcommand_from init; and not __fish_seen_subcommand_from query; and not __fish_seen_subcommand_from fonts; and not __fish_seen_subcommand_from update; and not __fish_seen_subcommand_from help" -f -a "help" -d 'Print this message or the help of the given subcommand(s)'

#compdef typst

autoload -U is-at-least

_typst() {
    typeset -A opt_args
    typeset -a _arguments_options
    local ret=1

    if is-at-least 5.2; then
        _arguments_options=(-s -S -C)
    else
        _arguments_options=(-s -C)
    fi

    local context curcontext="$curcontext" state line
    _arguments "${_arguments_options[@]}" \
'--color=[Set when to use color. auto = use color if a capable terminal is detected]' \
'--cert=[Path to a custom CA certificate to use when making network requests]:CERT:_files' \
'-h[Print help]' \
'--help[Print help]' \
'-V[Print version]' \
'--version[Print version]' \
":: :_typst_commands" \
"*::: :->typst" \
&& ret=0
    case $state in
    (typst)
        words=($line[1] "${words[@]}")
        (( CURRENT += 1 ))
        curcontext="${curcontext%:*:*}:typst-command-$line[1]:"
        case $line[1] in
            (compile)
_arguments "${_arguments_options[@]}" \
'--root=[Configures the project root (for absolute paths)]:DIR:_files' \
'*--input=[Add a string key-value pair visible through \`sys.inputs\`]:key=value: ' \
'*--font-path=[Adds additional directories to search for fonts]:DIR:_files' \
'--diagnostic-format=[The format to emit diagnostics in]:DIAGNOSTIC_FORMAT:(human short)' \
'-f+[The format of the output file, inferred from the extension by default]:FORMAT:(pdf png svg)' \
'--format=[The format of the output file, inferred from the extension by default]:FORMAT:(pdf png svg)' \
'--open=[Opens the output file using the default viewer after compilation]' \
'--ppi=[The PPI (pixels per inch) to use for PNG export]:PPI: ' \
'--timings=[Produces performance timings of the compilation process (experimental)]' \
'-h[Print help (see more with '\''--help'\'')]' \
'--help[Print help (see more with '\''--help'\'')]' \
':input -- Path to input Typst file, use `-` to read input from stdin:' \
'::output -- Path to output file (PDF, PNG, or SVG):_files' \
&& ret=0
;;
(watch)
_arguments "${_arguments_options[@]}" \
'--root=[Configures the project root (for absolute paths)]:DIR:_files' \
'*--input=[Add a string key-value pair visible through \`sys.inputs\`]:key=value: ' \
'*--font-path=[Adds additional directories to search for fonts]:DIR:_files' \
'--diagnostic-format=[The format to emit diagnostics in]:DIAGNOSTIC_FORMAT:(human short)' \
'-f+[The format of the output file, inferred from the extension by default]:FORMAT:(pdf png svg)' \
'--format=[The format of the output file, inferred from the extension by default]:FORMAT:(pdf png svg)' \
'--open=[Opens the output file using the default viewer after compilation]' \
'--ppi=[The PPI (pixels per inch) to use for PNG export]:PPI: ' \
'--timings=[Produces performance timings of the compilation process (experimental)]' \
'-h[Print help (see more with '\''--help'\'')]' \
'--help[Print help (see more with '\''--help'\'')]' \
':input -- Path to input Typst file, use `-` to read input from stdin:' \
'::output -- Path to output file (PDF, PNG, or SVG):_files' \
&& ret=0
;;
(init)
_arguments "${_arguments_options[@]}" \
'-h[Print help (see more with '\''--help'\'')]' \
'--help[Print help (see more with '\''--help'\'')]' \
':template -- The template to use, e.g. `@preview/charged-ieee`:' \
'::dir -- The project directory, defaults to the template'\''s name:' \
&& ret=0
;;
(query)
_arguments "${_arguments_options[@]}" \
'--root=[Configures the project root (for absolute paths)]:DIR:_files' \
'*--input=[Add a string key-value pair visible through \`sys.inputs\`]:key=value: ' \
'*--font-path=[Adds additional directories to search for fonts]:DIR:_files' \
'--diagnostic-format=[The format to emit diagnostics in]:DIAGNOSTIC_FORMAT:(human short)' \
'--field=[Extracts just one field from all retrieved elements]:FIELD: ' \
'--format=[The format to serialize in]:FORMAT:(json yaml)' \
'--one[Expects and retrieves exactly one element]' \
'-h[Print help]' \
'--help[Print help]' \
':input -- Path to input Typst file, use `-` to read input from stdin:' \
':selector -- Defines which elements to retrieve:' \
&& ret=0
;;
(fonts)
_arguments "${_arguments_options[@]}" \
'*--font-path=[Adds additional directories to search for fonts]:DIR:_files' \
'--variants[Also lists style variants of each font family]' \
'-h[Print help]' \
'--help[Print help]' \
&& ret=0
;;
(update)
_arguments "${_arguments_options[@]}" \
'--force[Forces a downgrade to an older version (required for downgrading)]' \
'--revert[Reverts to the version from before the last update (only possible if \`typst update\` has previously ran)]' \
'-h[Print help]' \
'--help[Print help]' \
'::version -- Which version to update to (defaults to latest):' \
&& ret=0
;;
(help)
_arguments "${_arguments_options[@]}" \
":: :_typst__help_commands" \
"*::: :->help" \
&& ret=0

    case $state in
    (help)
        words=($line[1] "${words[@]}")
        (( CURRENT += 1 ))
        curcontext="${curcontext%:*:*}:typst-help-command-$line[1]:"
        case $line[1] in
            (compile)
_arguments "${_arguments_options[@]}" \
&& ret=0
;;
(watch)
_arguments "${_arguments_options[@]}" \
&& ret=0
;;
(init)
_arguments "${_arguments_options[@]}" \
&& ret=0
;;
(query)
_arguments "${_arguments_options[@]}" \
&& ret=0
;;
(fonts)
_arguments "${_arguments_options[@]}" \
&& ret=0
;;
(update)
_arguments "${_arguments_options[@]}" \
&& ret=0
;;
(help)
_arguments "${_arguments_options[@]}" \
&& ret=0
;;
        esac
    ;;
esac
;;
        esac
    ;;
esac
}

(( $+functions[_typst_commands] )) ||
_typst_commands() {
    local commands; commands=(
'compile:Compiles an input file into a supported output format' \
'c:Compiles an input file into a supported output format' \
'watch:Watches an input file and recompiles on changes' \
'w:Watches an input file and recompiles on changes' \
'init:Initializes a new project from a template' \
'query:Processes an input file to extract provided metadata' \
'fonts:Lists all discovered fonts in system and custom font paths' \
'update:Self update the Typst CLI (disabled)' \
'help:Print this message or the help of the given subcommand(s)' \
    )
    _describe -t commands 'typst commands' commands "$@"
}
(( $+functions[_typst__compile_commands] )) ||
_typst__compile_commands() {
    local commands; commands=()
    _describe -t commands 'typst compile commands' commands "$@"
}
(( $+functions[_typst__help__compile_commands] )) ||
_typst__help__compile_commands() {
    local commands; commands=()
    _describe -t commands 'typst help compile commands' commands "$@"
}
(( $+functions[_typst__fonts_commands] )) ||
_typst__fonts_commands() {
    local commands; commands=()
    _describe -t commands 'typst fonts commands' commands "$@"
}
(( $+functions[_typst__help__fonts_commands] )) ||
_typst__help__fonts_commands() {
    local commands; commands=()
    _describe -t commands 'typst help fonts commands' commands "$@"
}
(( $+functions[_typst__help_commands] )) ||
_typst__help_commands() {
    local commands; commands=(
'compile:Compiles an input file into a supported output format' \
'watch:Watches an input file and recompiles on changes' \
'init:Initializes a new project from a template' \
'query:Processes an input file to extract provided metadata' \
'fonts:Lists all discovered fonts in system and custom font paths' \
'update:Self update the Typst CLI (disabled)' \
'help:Print this message or the help of the given subcommand(s)' \
    )
    _describe -t commands 'typst help commands' commands "$@"
}
(( $+functions[_typst__help__help_commands] )) ||
_typst__help__help_commands() {
    local commands; commands=()
    _describe -t commands 'typst help help commands' commands "$@"
}
(( $+functions[_typst__help__init_commands] )) ||
_typst__help__init_commands() {
    local commands; commands=()
    _describe -t commands 'typst help init commands' commands "$@"
}
(( $+functions[_typst__init_commands] )) ||
_typst__init_commands() {
    local commands; commands=()
    _describe -t commands 'typst init commands' commands "$@"
}
(( $+functions[_typst__help__query_commands] )) ||
_typst__help__query_commands() {
    local commands; commands=()
    _describe -t commands 'typst help query commands' commands "$@"
}
(( $+functions[_typst__query_commands] )) ||
_typst__query_commands() {
    local commands; commands=()
    _describe -t commands 'typst query commands' commands "$@"
}
(( $+functions[_typst__help__update_commands] )) ||
_typst__help__update_commands() {
    local commands; commands=()
    _describe -t commands 'typst help update commands' commands "$@"
}
(( $+functions[_typst__update_commands] )) ||
_typst__update_commands() {
    local commands; commands=()
    _describe -t commands 'typst update commands' commands "$@"
}
(( $+functions[_typst__help__watch_commands] )) ||
_typst__help__watch_commands() {
    local commands; commands=()
    _describe -t commands 'typst help watch commands' commands "$@"
}
(( $+functions[_typst__watch_commands] )) ||
_typst__watch_commands() {
    local commands; commands=()
    _describe -t commands 'typst watch commands' commands "$@"
}

if [ "$funcstack[1]" = "_typst" ]; then
    _typst "$@"
else
    compdef _typst typst
fi

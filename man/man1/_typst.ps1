
using namespace System.Management.Automation
using namespace System.Management.Automation.Language

Register-ArgumentCompleter -Native -CommandName 'typst' -ScriptBlock {
    param($wordToComplete, $commandAst, $cursorPosition)

    $commandElements = $commandAst.CommandElements
    $command = @(
        'typst'
        for ($i = 1; $i -lt $commandElements.Count; $i++) {
            $element = $commandElements[$i]
            if ($element -isnot [StringConstantExpressionAst] -or
                $element.StringConstantType -ne [StringConstantType]::BareWord -or
                $element.Value.StartsWith('-') -or
                $element.Value -eq $wordToComplete) {
                break
        }
        $element.Value
    }) -join ';'

    $completions = @(switch ($command) {
        'typst' {
            [CompletionResult]::new('--color', 'color', [CompletionResultType]::ParameterName, 'Set when to use color. auto = use color if a capable terminal is detected')
            [CompletionResult]::new('--cert', 'cert', [CompletionResultType]::ParameterName, 'Path to a custom CA certificate to use when making network requests')
            [CompletionResult]::new('-h', 'h', [CompletionResultType]::ParameterName, 'Print help')
            [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'Print help')
            [CompletionResult]::new('-V', 'V ', [CompletionResultType]::ParameterName, 'Print version')
            [CompletionResult]::new('--version', 'version', [CompletionResultType]::ParameterName, 'Print version')
            [CompletionResult]::new('compile', 'compile', [CompletionResultType]::ParameterValue, 'Compiles an input file into a supported output format')
            [CompletionResult]::new('watch', 'watch', [CompletionResultType]::ParameterValue, 'Watches an input file and recompiles on changes')
            [CompletionResult]::new('init', 'init', [CompletionResultType]::ParameterValue, 'Initializes a new project from a template')
            [CompletionResult]::new('query', 'query', [CompletionResultType]::ParameterValue, 'Processes an input file to extract provided metadata')
            [CompletionResult]::new('fonts', 'fonts', [CompletionResultType]::ParameterValue, 'Lists all discovered fonts in system and custom font paths')
            [CompletionResult]::new('update', 'update', [CompletionResultType]::ParameterValue, 'Self update the Typst CLI (disabled)')
            [CompletionResult]::new('help', 'help', [CompletionResultType]::ParameterValue, 'Print this message or the help of the given subcommand(s)')
            break
        }
        'typst;compile' {
            [CompletionResult]::new('--root', 'root', [CompletionResultType]::ParameterName, 'Configures the project root (for absolute paths)')
            [CompletionResult]::new('--input', 'input', [CompletionResultType]::ParameterName, 'Add a string key-value pair visible through `sys.inputs`')
            [CompletionResult]::new('--font-path', 'font-path', [CompletionResultType]::ParameterName, 'Adds additional directories to search for fonts')
            [CompletionResult]::new('--diagnostic-format', 'diagnostic-format', [CompletionResultType]::ParameterName, 'The format to emit diagnostics in')
            [CompletionResult]::new('-f', 'f', [CompletionResultType]::ParameterName, 'The format of the output file, inferred from the extension by default')
            [CompletionResult]::new('--format', 'format', [CompletionResultType]::ParameterName, 'The format of the output file, inferred from the extension by default')
            [CompletionResult]::new('--open', 'open', [CompletionResultType]::ParameterName, 'Opens the output file using the default viewer after compilation')
            [CompletionResult]::new('--ppi', 'ppi', [CompletionResultType]::ParameterName, 'The PPI (pixels per inch) to use for PNG export')
            [CompletionResult]::new('--timings', 'timings', [CompletionResultType]::ParameterName, 'Produces performance timings of the compilation process (experimental)')
            [CompletionResult]::new('-h', 'h', [CompletionResultType]::ParameterName, 'Print help (see more with ''--help'')')
            [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'Print help (see more with ''--help'')')
            break
        }
        'typst;watch' {
            [CompletionResult]::new('--root', 'root', [CompletionResultType]::ParameterName, 'Configures the project root (for absolute paths)')
            [CompletionResult]::new('--input', 'input', [CompletionResultType]::ParameterName, 'Add a string key-value pair visible through `sys.inputs`')
            [CompletionResult]::new('--font-path', 'font-path', [CompletionResultType]::ParameterName, 'Adds additional directories to search for fonts')
            [CompletionResult]::new('--diagnostic-format', 'diagnostic-format', [CompletionResultType]::ParameterName, 'The format to emit diagnostics in')
            [CompletionResult]::new('-f', 'f', [CompletionResultType]::ParameterName, 'The format of the output file, inferred from the extension by default')
            [CompletionResult]::new('--format', 'format', [CompletionResultType]::ParameterName, 'The format of the output file, inferred from the extension by default')
            [CompletionResult]::new('--open', 'open', [CompletionResultType]::ParameterName, 'Opens the output file using the default viewer after compilation')
            [CompletionResult]::new('--ppi', 'ppi', [CompletionResultType]::ParameterName, 'The PPI (pixels per inch) to use for PNG export')
            [CompletionResult]::new('--timings', 'timings', [CompletionResultType]::ParameterName, 'Produces performance timings of the compilation process (experimental)')
            [CompletionResult]::new('-h', 'h', [CompletionResultType]::ParameterName, 'Print help (see more with ''--help'')')
            [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'Print help (see more with ''--help'')')
            break
        }
        'typst;init' {
            [CompletionResult]::new('-h', 'h', [CompletionResultType]::ParameterName, 'Print help (see more with ''--help'')')
            [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'Print help (see more with ''--help'')')
            break
        }
        'typst;query' {
            [CompletionResult]::new('--root', 'root', [CompletionResultType]::ParameterName, 'Configures the project root (for absolute paths)')
            [CompletionResult]::new('--input', 'input', [CompletionResultType]::ParameterName, 'Add a string key-value pair visible through `sys.inputs`')
            [CompletionResult]::new('--font-path', 'font-path', [CompletionResultType]::ParameterName, 'Adds additional directories to search for fonts')
            [CompletionResult]::new('--diagnostic-format', 'diagnostic-format', [CompletionResultType]::ParameterName, 'The format to emit diagnostics in')
            [CompletionResult]::new('--field', 'field', [CompletionResultType]::ParameterName, 'Extracts just one field from all retrieved elements')
            [CompletionResult]::new('--format', 'format', [CompletionResultType]::ParameterName, 'The format to serialize in')
            [CompletionResult]::new('--one', 'one', [CompletionResultType]::ParameterName, 'Expects and retrieves exactly one element')
            [CompletionResult]::new('-h', 'h', [CompletionResultType]::ParameterName, 'Print help')
            [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'Print help')
            break
        }
        'typst;fonts' {
            [CompletionResult]::new('--font-path', 'font-path', [CompletionResultType]::ParameterName, 'Adds additional directories to search for fonts')
            [CompletionResult]::new('--variants', 'variants', [CompletionResultType]::ParameterName, 'Also lists style variants of each font family')
            [CompletionResult]::new('-h', 'h', [CompletionResultType]::ParameterName, 'Print help')
            [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'Print help')
            break
        }
        'typst;update' {
            [CompletionResult]::new('--force', 'force', [CompletionResultType]::ParameterName, 'Forces a downgrade to an older version (required for downgrading)')
            [CompletionResult]::new('--revert', 'revert', [CompletionResultType]::ParameterName, 'Reverts to the version from before the last update (only possible if `typst update` has previously ran)')
            [CompletionResult]::new('-h', 'h', [CompletionResultType]::ParameterName, 'Print help')
            [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'Print help')
            break
        }
        'typst;help' {
            [CompletionResult]::new('compile', 'compile', [CompletionResultType]::ParameterValue, 'Compiles an input file into a supported output format')
            [CompletionResult]::new('watch', 'watch', [CompletionResultType]::ParameterValue, 'Watches an input file and recompiles on changes')
            [CompletionResult]::new('init', 'init', [CompletionResultType]::ParameterValue, 'Initializes a new project from a template')
            [CompletionResult]::new('query', 'query', [CompletionResultType]::ParameterValue, 'Processes an input file to extract provided metadata')
            [CompletionResult]::new('fonts', 'fonts', [CompletionResultType]::ParameterValue, 'Lists all discovered fonts in system and custom font paths')
            [CompletionResult]::new('update', 'update', [CompletionResultType]::ParameterValue, 'Self update the Typst CLI (disabled)')
            [CompletionResult]::new('help', 'help', [CompletionResultType]::ParameterValue, 'Print this message or the help of the given subcommand(s)')
            break
        }
        'typst;help;compile' {
            break
        }
        'typst;help;watch' {
            break
        }
        'typst;help;init' {
            break
        }
        'typst;help;query' {
            break
        }
        'typst;help;fonts' {
            break
        }
        'typst;help;update' {
            break
        }
        'typst;help;help' {
            break
        }
    })

    $completions.Where{ $_.CompletionText -like "$wordToComplete*" } |
        Sort-Object -Property ListItemText
}
